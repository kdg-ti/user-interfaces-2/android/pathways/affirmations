package com.example.pwaffirmations

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.pwaffirmations.databinding.ActivityMainBinding
import com.example.pwaffirmations.model.ItemAdapter
import java.util.*

class MainActivity : AppCompatActivity() {
  private lateinit var binding: ActivityMainBinding
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityMainBinding.inflate(layoutInflater)
    setContentView(binding.root)
    initialiseViews()
  }

  private fun initialiseViews() {
    binding.affirmations.adapter =
      ItemAdapter(resources.getStringArray(R.array.affirmations).toList())
    binding.affirmations.setHasFixedSize(true)
  }
}