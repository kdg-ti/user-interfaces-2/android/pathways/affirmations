package com.example.pwaffirmations.model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pwaffirmations.databinding.ListItemBinding

class ItemAdapter( private val affirmations:List<String>):RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
//  class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
//    val textView: TextView = view.findViewById(R.id.item_title)
//  }
  class ItemViewHolder( val binding: ListItemBinding):RecyclerView.ViewHolder(binding.root)

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
    //return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item,parent,false)))
    return ItemViewHolder(ListItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
  }

  override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
//    holder.textView.text = affirmations.get(position)

    holder.binding.itemTitle.text=affirmations.get(position)
  }

  override fun getItemCount(): Int {
    return affirmations.size
  }
}